# Battle: The Game of Generals

[[_TOC_]]

This is my remake of the wargame [_Battle: The Game of
Generals_](https://boardgamegeek.com/boardgame/4299/battle-game-strategy).
The original game was published by Yaquinto Publications, Inc. in
1979, and later republished as _Battle: The Game of Strategy_, also by
Yaquinto Publications, Inc., in 1981.  The game has since gone out of
print.

The game was later re-implemented in [_Battle: The American Civil
War_](https://boardgamegeek.com/boardgame/27583/battle-american-civil-war)
in 2006 and [_Battle: The Napoleonic
Wars_](https://boardgamegeek.com/boardgame/38732/battle-napoleonic-wars)
in 2008, albeit that these were much less abstract, and instead of a
fixed board, the board was made up of hexagon tiles.  Both of these
games has since gone out of print. 

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | Multi              |
| Level        | Operational        |
| Hex scale    | Varies             |
| Unit scale   | corps (xxx)        |
| Turn scale   | Varies             |
| Unit density | Medium             |
| # of turns   | Varies             |
| Complexity   | 2 of 10            |
| Solitaire    | 6 of 10            |

Features:
- Abstract
- Multiple periods (ancient to modern)
- Scenarios
- Optional rules

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The files 

The distribution consists of the following files 

- [BattleA4.pdf][] This is a single document that contains everything:
  The rules, board, cards, counters, and orders of battle.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [BattleA4Booklet.pdf][] This document _only_ contains the rules.  It
  is meant to be printed on a duplex A4 printer with long edge
  binding.  This will produce 3 sheets which should be folded down the
  middle of the long edge, and stabled to form an 12-page A5 booklet
  of the rules.
  
- [materialsA4.pdf][] holds the board, cards, counters, and OOBs.  It
  is meant to be printed on A4 paper.  Print and glue on to a
  relatively thick piece of cardboard (1.5mm or so) or the like.

If you only have access to US Letter printer, you can use the
following files

| *A4 Series*             | *Letter Series*             |
| ------------------------|-----------------------------|
| [BattleA4.pdf][]        | [BattleLetter.pdf][]  	    |
| [BattleA4Booklet.pdf][] | [BattleLetterBooklet.pdf][] |
| [materialsA4.pdf][]	  | [materialsLetter.pdf][]	    |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL modules 

Also available as a [VASSAL](https://vassalengine.org) module

- [`Battle.vmod`][Battle.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  It has
predefined tiles and HQ setups, automatic chess clock, and so on. 

## Articles 

- [Scan of pages in GAMES May/June 1981](https://boardgamegeek.com/thread/1967463/games-magazine-back-issues#38420941)
- [Board Game Geek
  page](https://boardgamegeek.com/boardgame/4299/battle-game-strategy) 

## Previews

![Board](.imgs/hexes.png)
![Front cover](.imgs/front.png)
![Counters and tiles](.imgs/counters.png)
![VASSAL module](.imgs/vassal.png)
![Components](.imgs/overview.jpg)
![Blue setup](.imgs/blue.jpg)
![Red setup](.imgs/red.jpg)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 

[artifacts.zip]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/download?job=dist

[BattleA4.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-A4-master/BattleA4.pdf?job=dist
[BattleA4Booklet.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-A4-master/BattleA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-A4-master/materialsA4.pdf?job=dist
[Battle.vmod]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-A4-master/Battle.vmod?job=dist

[BattleLetter.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-Letter-master/BattleLetter.pdf?job=dist
[BattleLetterBooklet.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-Letter-master/BattleLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/battle_tex/-/jobs/artifacts/master/file/Battle-Letter-master/materialsLetter.pdf?job=dist
