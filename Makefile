#
#
#
NAME		:= Battle
VERSION		:= 1.0
VMOD_VERSION	:= 1.0
LATEX		:= lualatex
LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			\
		   -shell-escape
EXPORT          := $(shell kpsewhich wgexport.py)
EXPORT_FLAGS	:=
PDFTOCAIRO	:= pdftocairo
CAIROFLAGS	:= -scale-to 800
PDFJAM		:= pdfjam
REDIR		:= > /dev/null 2>&1 
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   rules.tex			\
		   periods.tex			\
		   materials.tex		
DATAFILES	:= counters.tex			\
		   armies.tex			\
		   hexes.tex
OTHER		:= 
STYFILES	:= battle.sty			\
		   commonwg.sty
BOARDS		:= hexes.pdf			
SIGNATURE	:= 12
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf 		\
		   materials.pdf		\
		   hexes.pdf

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   materialsA4.pdf		\
		   hexes.pdf			

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   materialsLetter.pdf		
SUBMAKE_FLAGS	:= --no-print-directory
ifdef VERBOSE	
MUTE		:=
REDIR		:=
SUBMAKE_FLAGS	:=
LATEX_FLAGS	:= -synctex 15	-shell-escape
EXPORT_FLAGS	:= -V
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif

%.pdf:	%.tex
	@echo "LATEX	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%A4.aux:%.tex
	@echo "LATEX(1)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%.tex
	@echo "LATEX	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%Letter.aux:	%.tex
	@echo "LATEX(1)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%.tex
	@echo "LATEX	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Rules.aux:	%.tex
	@echo "LATEX(1)	$*Rules"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Rules $* $(REDIR)

%Rules.pdf:	%Rules.aux
	@echo "LATEX(2)	$*Rules"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Rules $* $(REDIR)

%Booklet.pdf:%.pdf
	@echo "BOOKLET	$*"
	$(MUTE)$(PDFJAM) 			\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

%.png:%-pdfjam.pdf
	@echo "PDFTOCAIRO $* (via PDFJam)"
	$(MUTE)rm -f $@
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $* $(REDIR)

%.png:%.pdf
	@echo "PDFTOCAIRO $*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	@echo "PDFTOCAIRO $*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

all:	$(TARGETS)
a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
letter:	us
vmod:	$(NAME).vmod
mine:	a4 vmod 

distdir:$(TARGETS) README.md
	@echo "DISTDIR	$(NAME)-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)cp $^ $(NAME)-$(VERSION)/
	$(MUTE)touch $(NAME)-$(VERSION)/*

dist:	distdir
	@echo "DIST	$(NAME)-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distdirA4:$(TARGETSA4) README.md
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distA4:	distdirA4
	@echo "DIST	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirLetter:$(TARGETSLETTER) README.md
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-Letter-$(VERSION)
	$(MUTE)cp $^ $(NAME)-Letter-$(VERSION)/
	$(MUTE)touch $(NAME)-Letter-$(VERSION)/*

distLetter:	distdirLetter
	@echo "DIST	$(NAME)-Letter-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-Letter-$(VERSION).zip $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)

clean:
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf *-pdfjam.pdf *.vmod *_out.tex *.json
	$(MUTE)rm -f splitboard*.pdf calcsplit*.pdf .oldschool
	$(MUTE)rm -f board.png board.svg logo.png tables.png
	$(MUTE)rm -f tables.png counters.png front.png crossings.tex
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSLETTER)
	$(MUTE)rm -f wargame.zip

realclean: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)
	$(MUTE)rm -rf oldschool* __pycache__

distclean:realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

# These generate images that are common for all formats 
hexes.pdf:		hexes.tex	$(STYFILES)

$(NAME).pdf:		$(NAME).aux
$(NAME).aux:		$(DOCFILES) 	$(STYFILES)	$(BOARDS)
materials.pdf:		materials.tex	$(DATAFILES)	$(BOARDS)

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) 	$(BOARDS)
materialsA4.pdf:	materials.tex 	$(DATAFILES) 	$(BOARDS)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	$(BOARDS)
materialsLetter.pdf:	materials.tex 	$(DATAFILES) 	$(BOARDS)

# Generic dependency

front.pdf:		front.tex       $(STYFILES)	$(BOARDS)
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper

export.pdf:	export.tex $(DATAFILES) front.pdf $(STYFILES)

# Temporary (chit scale 0.85 perhaps)
$(NAME).vmod:export.pdf patch.py $(NAME)Rules.pdf $(TUTORIAL)
	@echo "VMOD	$@"
	$(MUTE)$(EXPORT) export.pdf export.json 		\
		-p patch.py 					\
		-r $(NAME)Rules.pdf				\
		-v $(VMOD_VERSION) 				\
		-t "Battle" 					\
		-d "From LaTeX sources" 			\
		-S 1						\
		$(TUTORIAL:%.vlog=-T %.vlog)			\
		-o $@  $(EXPORT_FLAGS) $(REDIR)

docker:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash

docker-prep:
	apt update
	apt install -y wget python3-pil poppler-utils 
	wget "https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist" -O wargame.zip
	unzip -o wargame.zip -d ${HOME}
	rm -rf $(HOME)/texmf
	mv ${HOME}/public ${HOME}/texmf

#mktextfm frunmn
#mktextfm frunbn

docker-build:docker-prep
	make docker-artifacts \
		VERSION=${CI_COMMIT_REF_NAME} \
		VERBOSE=1 \
		LATEX_FLAGS=

docker-artifacts: distdirA4 distdirLetter vmod
	cp $(NAME).vmod $(NAME)-A4-$(VERSION)/

.PRECIOUS:	export.pdf $(NAME)Rules.pdf

#
# EOF
#

