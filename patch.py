from wgexport import *

## -------------------------------------------------------------------
moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2>Features</h2>

  <p>The turn tracker is useful for keeping track of the progress of
   the game. If enable in the preferences, then switching between
   phases automatically starts and pauses the chess clock.  It also
   disables loading a new terrain setup after the initial
   setup. Switch to the next phase by pressing <code>Alt-T</code>.</p>

  <p>The terrain setup button (a hex) will give you options for
   loading a tile setup, including HQ placements. You can switch
   between the setups, although if you chose <b>Alternative C</b>,
   then the tiles have been rotated and the other setups will not load
   correctly. Bring up the menu by pressing <code>Alt-Z</code>.</p>

  <p>The OOBs show the different army configurations for different
   periods.  Once a period has been chosen, then the units should be
   moved to the map for setup. The OOBs can be brought up by pressing
   <code>Alt-B</code>.

  <h2>About this game</h2>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
   sources of a Print'n'Play version of the <b>D-Day</b> game.  That
   (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/battle_tex</code></a>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.  </p>

  <p> The original game was release by the Avalon Hill Game
   Company.</p>

  <h2>Credits</h2>
  <dl>
   <dt>Design &amp; development:</dt>
   <dd>Samuel Craig Taylor, Jr.</dd>
   <dt>Production coordination:</dt>
   <dd>J.Stephen Peek</dd>
   <dt>Playtesters:</dt>
   <dd>Gene Baker, Nolan Bond, Larry Brom, John Castro, Chris Cornaghie,
    Kevin Duke, Dave Furguson, John Fuseler, Willam Glankler, Jim
    Henson, Scott Majeske, Steve Peek, David P. Smith, John T. White,
    Jack Young, &amp; Wayne Lenham.
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# ----------------------------------------------------------------
setups = {
    'Standard': {
        'town 1':       { 'hex': '0302', 'rotation': 0 },# 0302
        'town 2':       { 'hex': '0910', 'rotation': 0 },
        'small hill 1': { 'hex': '0705', 'rotation': 0 },
        'small hill 2': { 'hex': '0507', 'rotation': 0 },
        'large hill 1': { 'hex': '0907', 'rotation': 0 },
        'large hill 2': { 'hex': '0305', 'rotation': 0 },
        'woods 1':      { 'hex': '0803', 'rotation': 0 },
        'woods 2':      { 'hex': '0410', 'rotation': 0 },
        'r hq':         { 'hex': '1002', 'rotation': 0 },
        'b hq':         { 'hex': '0211', 'rotation': 0 },
    },
    'Alternative A': {
        'town 1':       { 'hex': '0603', 'rotation': 0 },# 0302
        'town 2':       { 'hex': '0610', 'rotation': 0 },
        'small hill 1': { 'hex': '0506', 'rotation': 0 },
        'small hill 2': { 'hex': '0706', 'rotation': 0 },
        'large hill 1': { 'hex': '0705', 'rotation': 0 },
        'large hill 2': { 'hex': '0507', 'rotation': 0 },
        'woods 1':      { 'hex': '0203', 'rotation': 0 },
        'woods 2':      { 'hex': '1010', 'rotation': 0 },
        'r hq':         { 'hex': '0202', 'rotation': 0 },
        'b hq':         { 'hex': '1011', 'rotation': 0 },
    },
    'Alternative B': {
        'town 1':       { 'hex': '0606', 'rotation': 0 },# 0302
        'town 2':       { 'hex': '0607', 'rotation': 0 },
        'small hill 1': { 'hex': '0603', 'rotation': 0 },
        'small hill 2': { 'hex': '0610', 'rotation': 0 },
        'large hill 1': { 'hex': '0609', 'rotation': 0 },
        'large hill 2': { 'hex': '0604', 'rotation': 0 },
        'woods 1':      { 'hex': '0206', 'rotation': 0 },
        'woods 2':      { 'hex': '1007', 'rotation': 0 },
        'r hq':         { 'hex': '0202', 'rotation': 0 },
        'b hq':         { 'hex': '1011', 'rotation': 0 },
    },
    'Alternative C': {
        'town 1':       { 'hex': '0903', 'rotation': 0 },# 0302
        'town 2':       { 'hex': '0309', 'rotation': 0 },
        'small hill 1': { 'hex': '0507', 'rotation': 1 },
        'small hill 2': { 'hex': '0607', 'rotation': 1 },
        'large hill 1': { 'hex': '0610', 'rotation': 0 },
        'large hill 2': { 'hex': '0603', 'rotation': 0 },
        'woods 1':      { 'hex': '0205', 'rotation': 0 },
        'woods 2':      { 'hex': '1008', 'rotation': 0 },
        'r hq':         { 'hex': '0602', 'rotation': 0 },
        'b hq':         { 'hex': '0611', 'rotation': 0 },
    },
}
offsets = {
    'town 1':       [0,0],
    'town 2':       [0,0],
    'small hill 1': [0,0], #0,-dy/2
    'small hill 2': [0,0], #0,+dy/2
    'large hill 1': [0,0], #0,+dy/2
    'large hill 2': [0,0], #0,-dy/2
    'woods 1':      [0,0], #+dx/2,-dy/4
    'woods 2':      [0,0], #-dx/2,+dy/4
}
    

def setupKey(name):
    return key(NONE,0)+f',setup{name.replace(" ","")}'

def startKey(name):
    return key(NONE,0)+f',start{name.replace(" ","")}'
    
def _getXY(c,r,e,v,o,x0,y0,dx,dy):
    pass 

def patch(build,data,vmod,verbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    # ================================================================
    autoClock      = 'battleAutoClock'
    pauseCombat    = 'battlePauseForCombat'
    startRedClock  = startKey('Red')
    startBlueClock = startKey('Blue')
    pauseForCombat = key(NONE,0)+',pauseForCombat'
    hidden         = 'battle hidden'
    notSetup       = 'notSetup'
    setNotSetup    = key(NONE,0)+',setNotSetup'
    verb           = 'wgVerbose'
    debg           = 'wgDebug'

    # ================================================================
    game   = build.getGame()

    # ================================================================
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ================================================================
    clocks = game.addChessClock(pauseHotkey = pauseForCombat,
                                showTenths  = ChessClockControl.NEVER)
    for clockName, clock in clocks.getClocks().items():
        clock['icon']        = f'{clockName}-icon.png'
        clock['startHotkey'] = startKey(clockName)
    
    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Setup',
                     'Red movement',          # 0
                     'Red combat',            # 1
                     'Red advance',           # 2
                     'Blue movement',         # 3
                     'Blue combat',
                     'Blue advance']
    counter        = turns.getCounters()['Turn']
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'
    turns.addHotkey(hotkey       = key('T',ALT), # This works!
                    match        = f'{{Phase=="Setup"&&Turn>1}}',
                    name         = 'Skip set-up phase after turn 1')
    turns.addHotkey(hotkey       = startRedClock+'GKC',
                    match        = f'{{Phase=="Red movement"&&{autoClock}}}',
                    name         = 'Start Red chess clock',
                    reportFormat = f'{{{debg}?(Phase+": red clock"):""}}')
    turns.addHotkey(hotkey       = startBlueClock+'GKC',
                    match        = f'{{Phase=="Blue movement"&&{autoClock}}}',
                    name         = 'Start Blue chess clock',
                    reportFormat = f'{{{debg}?(Phase+": blue clock"):""}}')
    turns.addHotkey(hotkey       = pauseForCombat+'GKC',
                    match        = (f'{{Phase.contains("combat")&&'
                                    f'{autoClock}&&{pauseCombat}}}'),
                    name         = 'Pause clocs for combat',
                    reportFormat = f'{{{debg}?(Phase+": Pause clocks"):""}}')
    turns.addHotkey(hotkey       = setNotSetup+'GKC',
                    match        = f'{{Phase!="Setup"}}',
                    name         = 'Flag we are no longer setting up',
                    reportFormat = f'{{{debg}?(Phase+": No longer setup"):""}}')

    # ----------------------------------------------------------------
    #
    # Default preferences
    #
    go    = game.getGlobalOptions()[0]
    go.addBoolPreference(name    = autoClock,
                         default = True,
                         desc    = 'Automatically start and stop clocks',
                         tab     = game['name'])
    go.addBoolPreference(name    = pauseCombat,
                         default = True,
                         desc    = 'Pause clocks for combat',
                         tab     = game['name'])
    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    gp                = game.getGlobalProperties()[0];
    gp.addProperty(name         = notSetup,
                   initialValue = 'false',
                   isNumeric    = True,
                   description  = 'True when not in setup')
    # ----------------------------------------------------------------
    #
    # Global keys 
    #
    game.addGameMassKey(name         = 'Start Red clock',
                        buttonHotkey = startRedClock+'GKC',
                        hotkey       = startRedClock,
                        target       = '',
                        filter       = f'{{BasicName=="{hidden}"}}',
                        reportFormat = f'{{{debg}?("Start Red clock"):""}}')
    game.addGameMassKey(name         = 'Start Blue clock',
                        buttonHotkey = startBlueClock+'GKC',
                        hotkey       = startBlueClock,
                        target       = '',
                        filter       = f'{{BasicName=="{hidden}"}}',
                        reportFormat = f'{{{debg}?("Start Blue clock"):""}}')
    game.addGameMassKey(name         = 'Pause all clocks',
                        buttonHotkey = pauseForCombat+'GKC',
                        hotkey       = pauseForCombat,
                        target       = '',
                        filter       = f'{{BasicName=="{hidden}"}}',
                        reportFormat = f'{{{debg}?("Pause for combat"):""}}')
    game.addGameMassKey(name         = 'Flag no longer setup',
                        buttonHotkey = setNotSetup+'GKC',
                        hotkey       = setNotSetup,
                        target       = '',
                        filter       = f'{{BasicName=="{hidden}"}}',
                        reportFormat = f'{{{debg}?("No longer in setup"):""}}')
    
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']
    
    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones            = zoned.getZones()
    hzone            = zones['hexes']
    hgrids           = hzone.getHexGrids()
    hgrid            = hgrids[0]
    #hgrid['visible'] = True
    hgrid['y0']      = int(hgrid['y0'])+int(float(hgrid['dy'])//2)
    hnum             = hgrid.getNumbering()[0]
    hnum['sep']      = '@' # Avoid stripping zeros 
    hnum['hOff']     = 1
    #hnum['visible']  = True
    hnum['stagger']  = str(True).lower()

    dx                      = float(hgrid['dx'])
    dy                      = float(hgrid['dy'])
    offsets['small hill 1'] = [0,    -dy/2]
    offsets['small hill 2'] = [0,    +dy/2]
    offsets['large hill 1'] = [0,    +dy/2]
    offsets['large hill 2'] = [0,    -dy/2]
    offsets['woods 1']      = [+dx/2,-dy/4]
    offsets['woods 2']      = [-dx/2,+dy/4]
    
    print(hgrid['x0'],hgrid['y0'],hgrid['dx'],hgrid['dy'],hnum['stagger'])

    # ----------------------------------------------------------------
    # Closure to get XY coordiantes from grid coordiantes 
    def getXY(c,r,v=None,e=None,o=1):
        return _getXY(c,r,v,e,o,
                      int(hgrid['x0']),   int(grid['y0']),
                      float(hgrid['dx']), float(hgrid['dy']))

    # ----------------------------------------------------------------
    setupNames = []
    for name, where in setups.items():
        setupNames.append(name)
        kn = setupKey(name)
        game.addGameMassKey(name         = f'{name} terrain setup',
                            buttonHotkey = '',
                            buttonText   = name,
                            hotkey       = kn,
                            icon         = '',
                            singleMap    = False,
                            target       = '',
                            filter       = f'{{Type=="Tile"||Type=="headquarters"}}',
                            reportFormat = f'{{"Selected setup {name}"}}')
    game.addMenu(description  = 'Menu of setups',
                 hotkey       = key('Z',ALT),
                 menuItems    = setupNames,
                 text         = '',
                 icon         = 'Terrain-icon.png',
                 tooltip      = 'Select terrain setup',
                 canDisable   = True,
                 propertyGate = notSetup)
                        
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()    
    prototypes         = prototypeContainer[0].getPrototypes()

    red   = prototypes['Red prototype']
    blue  = prototypes['Blue prototype']
    tiles = prototypes['Marker prototype']
    
    for prototype in [red,blue]:
        traits = prototype.getTraits();
        basic  = traits.pop()
        delt   = Trait.findTrait(traits,DeleteTrait.ID)
        if delt: traits.remove(delt)

        prototype.setTraits(*traits,basic)

    # Handle the marker prototype.  Remove the rotation trait, which
    # we will add to the pieces directly so that we can set the
    # initial rotation angle.
    traits = tiles.getTraits()
    basic  = traits.pop()
    rot    = Trait.findTrait(traits,RotateTrait.ID)
    if rot: traits.remove(rot)
    traits.append(MarkTrait(name='Type',value='Tile'))
    traits.append(NoStackTrait(select      = NoStackTrait.SHIFT_SELECT,
                               bandSelect  = NoStackTrait.NEVER_BAND_SELECT,
                               move        = NoStackTrait.SELECT_MOVE,
                               ignoreGrid  = False,
                               description = 'Terrain tiles do not stack'))
    tiles.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    pieces    = game.getPieces(asdict=False)
    tileNames = ['town','small hill','large hill','woods']
    for piece in pieces:
        name   = piece['entryName'] 
        traits = piece.getTraits()
        basic  = traits.pop()
        empty  = Trait.findTrait(traits,PrototypeTrait.ID,
                                 key='name',
                                 value=' prototype')
        if empty: traits.remove(empty)

        # ... HQ .....................................................
        if 'hq' in name:
            # .. Send to location from setup command .................
            send = []
            rept = []
            for sname, specs in setups.items():
                spec = specs.get(name,None)
                if spec is None:
                    continue

                kn = setupKey(sname)
                hx = spec['hex']
                gr = hx[:2]+'@'+hx[2:]
                send.append(SendtoTrait(
                    mapName     = main['mapName'],
                    boardName   = main['mapName'],
                    key         = kn,
                    destination = SendtoTrait.GRID,
                    position    = gr))
                rept.append(ReportTrait(
                    kn,
                    report = (f'{{{debg}?("Sending {name} to {hx} "+'
                              f'"for {sname}"):""}}')))

            traits.extend(send)
            traits.extend(rept)
            
        # ... Tile ...................................................
        if any([t in name  for t in tileNames]):
            atstart = piece.getParent(AtStart)
            fn      = basic['filename']

            # .. Make non-rectangular and report rotation ............
            img     = vmod.getInternalFile('images/'+fn,'r').read()
            nrct    = NonRectangleTrait(filename=fn,image=img)
            traits.append(rot)  # Before non-rect?
            traits.append(nrct)
            traits.append(ReportTrait(
                key('['),key(']'),
                report = (f'{{{debg}?(BasicName+" rotated to face "+'
                          f'{rot["name"]}_Facing):""}}')))
                                      
            rot['angle'] = 0 # Reset to default (0 to 5, not 1 to 6 as
            # suggested by the documentation). 

            # .. Rotate these when placed in OBB .....................
            if atstart and name in ['woods 2','large hill 2']:
                print(f'Rotating {name} by 180 (index 3)')
                rot['angle'] = 3

            # .. Send to location from setup command .................
            trig = []
            send = []
            rept = []
            for sname, specs in setups.items():
                spec = specs.get(name,None)
                if spec is None:
                    continue

                xy = offsets[name]
                xo = xy[0]
                yo = xy[1]
                kn = setupKey(sname)
                hx = spec['hex']
                gr = hx[:2]+'@'+hx[2:]
                an = spec['rotation']
                ac = [kn+'Send']
                for nr in range(an):
                    #print(f'Rotate {name}')
                    #'['
                    ac.append(key(']'))
                    xo += dx/2
                    yo -= dy/2
                #print(ac)
                    
                trig.append(TriggerTrait(
                    name        = f'Trigger {sname} setup',
                    command     = '',
                    key         = kn,
                    actionKeys  = ac))
                send.append(SendtoTrait(
                    mapName     = main['mapName'],
                    boardName   = main['mapName'],
                    key         = kn+'Send',
                    xidx        = int(xo),
                    yidx        = int(yo),
                    destination = SendtoTrait.GRID,
                    position    = gr))
                rept.append(ReportTrait(
                    kn,
                    report = (f'{{{debg}?("Sending {name} to {hx} "+'
                              f'"for {sname} "+'
                              f'"(offset=({int(xo)},{int(yo)},"+'
                              f'"rotation={an})"):""}}'))
                            )

            traits.extend(send)
            traits.extend(trig)
            traits.extend(rept)

        # -- Hidden unit ---------------------------------------------
        if name == hidden:
            traits.extend([
                GlobalHotkeyTrait(
                    name         = '',
                    key          = startRedClock,
                    globalHotkey = startRedClock,
                    description  = 'Start red chess clock'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = startBlueClock,
                    globalHotkey = startBlueClock,
                    description  = 'Start blue chess clock'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = pauseForCombat,
                    globalHotkey = pauseForCombat,
                    description  = 'Pause clocks'),
                GlobalPropertyTrait(
                    ['',setNotSetup,GlobalPropertyTrait.DIRECT, f'{{true}}'],
                    name        = notSetup,
                    numeric     = True,
                    description = 'Set no longer in setup phase'),
                ReportTrait(
                    startRedClock,startBlueClock,pauseForCombat,
                    report=f'{{{debg}?(BasicName+": Manipulating clocs"):""}}'),
                ReportTrait(
                    setNotSetup,
                    report=f'{{{debg}?("Setting not setup: "+{notSetup}):""}}'),
            ])

                    
        piece.setTraits(*traits,basic)
    
#
# EOF
#

