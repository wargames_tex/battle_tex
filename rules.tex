\twocolumn[\part*{Rules}]

%% ===================================================================
\section{Introduction}
\label{sec:intro}

\textsl{Battle: A Game of Generals} is a simple wargame between two
factions - the blue and the red factions.   The game is abstract in
the sense that it does not simulate a specific conflict or battle, and
in that it can be used to simulate battles across the ages --- from
ancient to modern times.   The game is simple enough that it is easy
to learn, but complex enough that seasoned grognards may find it
enjoyable to play.

The object of each faction is to eliminate its opponents headquarters
represented by a headquarter unit.

%% ===================================================================
\section{Components}
\label{sec:components}

The game consist of

\begin{itemize}
\item These rules 
\item A board with a map on it.  The a grid of hexagons (hexes) on the
  map governs the manoeuvres of the military forces involved. 
\item 8 terrain tiles, 2 woods, 2 towns, 2 small hills, and 2 larger
  hills, to be placed in the board.  
\item 48 counters, each representing a military unit.  These units use
  standard NATO App6 symbology to represent which kind of military
  unit the counter represent.  The background of the units designates
  which faction it belongs to.
\item 7 period cards and Orders of Battle (OOBs).  These shows the
  configuration of armies available in each period.
\end{itemize}

%% -------------------------------------------------------------------
\subsection{The board and terrain}
\label{sec:components:board}

The board is $11\times12$ hexes big.  Each hex on the board proper
represent \emph{Clear} terrain.  Each hex has a coordinate printed on
it, so that it may be uniquely identified, for example when playing
via (e)mail.  There are two rivers on the board, each with a bridge
across.

The 8 terrain tiles are placed on the board to give the area of the
conflict some features that the factions may use.  How the terrain
tiles are placed depends on the scenario being played and what the two
factions agree upon before the start of the game.

The terrains and features are described in \tabref{tab:terrain}.  The
type of terrain or feature influence a unit's
\begin{itemize}
\item combat strength, both when attacking and defending, and
\item ability to enter a hex. 
\end{itemize}

\tikzset{
  tec/.style={
    scale=.5,
    transform shape},
}
\newdimen\tempdima
\begin{table}
  \tempdima=\linewidth
  \advance\tempdima-2.cm
  \setlength{\tabcolsep}{.2cm}
  \begin{tabular}{@{}|C{1.5cm}m{\tempdima}|@{}}
    \hline
    \defrow
    \tikz[tec]{\hex{}}
    & \textit{Clear}.  These hexes represent clear terrain
      where movement is relatively unhindered, and not a lot of
      possibilities for defensive stances.\\
    \altrow
    \tikz[tec]{\hex[terrain=rough,ridges={N,NE,SE,S,SW,NW}]{}}
    & \textit{Hills}.  Elevated terrain.  This provides in particular
      artillery some advantages and also some improved defences.\\
    \defrow
    \tikz[tec]{\hex[terrain=woods]{}}
    & \textit{Woods}.  Dense vegetation that disrupts lines of sight and
      manoeuvres. \\ 
    \altrow
    \tikz[tec]{\hex[terrain=town]{}}
    & \textit{Town}.  A small or large collection of houses and other
      structures.  This also disrupts lines of sight and provides many
      defensive opportunities. \\
    \defrow
    \tikz[tec]{\hex(c=0,r=0){}\river(hex cs:c=0,r=0,v=NW)
    --(hex cs:c=0,r=0,v=NE)
    --(hex cs:c=0,r=0,v=E)
    --(hex cs:c=0,r=0,v=SE);
    \pic[bridge,rotate=-60]at(hex cs:c=0,r=0,e=NE){natoapp6c/s/bridge};
    \node[bridge fill,rotate=-60] at (hex cs:c=0,r=0,e=NE){};
    }
    & \textit{River} and \textit{bridge}.  A body of water running a
      long hex edges. These are generally impassable \emph{except} via
      a bridge.
    \\
    \hline
  \end{tabular}
  \caption{Types of terrain and features}
  \label{tab:terrain}
\end{table}

Once the terrain tiles have been placed on the board they
\emph{cannot} not be moved anymore during the game.  The terrain is
permanent.

%% -------------------------------------------------------------------
\subsection{The counters}
\label{sec:components:counters}

Each of the 48 unit counters contains a symbol which indicates which
type of unit it is.  The symbols are the same for all periods (ancient
to modern) but reflect different types of units.  The unit symbols
and corresponding unit type per age is given in \tabref{tab:units}. 

The type of a unit has consequences for
\begin{itemize}
\item its combat strength, both when attacking and defending,
\item how fast it can move, and 
\item which kind of terrain it can enter. 
\end{itemize}
More details are given for each specific unit type for the specific
period.

What always applies is that headquarter units are special.  Not only
is it the objective of an opposing faction to conquer a factions
headquarters unit, but headquarter units
\begin{itemize}
\item has no combat strength (they are automatically eliminated if
  attacked), 
\item cannot move, and 
\item cannot occupy any terrain but \emph{Clear} terrain. 
\end{itemize}

Generally, units that move on foot (e.g, infantry) can move one hex,
mounted units (e.g, mechanised infantry, cavalry, armoured) can
move two, or if not attacking 3, hexes, and heavy equipment (e.g.,
artillery) can move one, or if not attacking two, hexes.   Heavy units
(e.g., cavalry, armoured, artillery) may typically \emph{not} enter or
occupy a \emph{Woods} hex.

%% -------------------------------------------------------------------
\subsection{Period cards}

There is one \emph{period card} for each period.  The card for a given
period summaries the available configurations of armies in the chosen
period.  An example of the basic period card is shown in
\figref{fig:card}.

\begin{figure*}
  \centering
  \begin{tikzpicture}
    \node[transform shape]{\basic};
  \end{tikzpicture}
  \caption{The basic period card}
  \label{fig:card}
\end{figure*}

The left most column shows which kinds of units are available in the
period, and the name used for the unit types.  The next four columns
tabulates the combat strength of each type of units when a unit of
that type occupies specific terrain.  The fifth \textbf{Move} column
indicates how many hexes a unit of a given type may move.  The
\textbf{Cost} column is only used if the factions decide to make their
own army configuration.  The final columns show how many of each kind
of unit is available to a faction.  Note, some period cards have
multiple configurations for armies.  The faction should decide which
configuration to use. 

%% ===================================================================
\section{Game flow}
\label{sec:flow}

The start the game, the factions must, in order, 
\begin{enumerate}
\item agree upon which period to play in, and which army setups to use,
\item agree upon which optional rules, if any, are used, 
\item set-up of the board, including placement of headquarters, and 
\item each faction setting up its units. 
\end{enumerate}
This is detailed more in \secref{sec:setup}. 

After this initial setup, the game progresses in \emph{turns}.  Each
turn is divided into to two sub-turns - first the red factions
sub-turn and then blue factions sub-turn.  Each factions sub-turn is
then further divided into three \emph{phases} which are in order

\begin{enumerate}
\item \textsl{Movement}.   The faction may move its units
  (\secref{sec:movement}). 
\item \textsl{Combat}.  The faction may conduct any attacks
  (\secref{sec:combat}). 
\item \textsl{Advance}.  The faction may advance victorious units into
  hexes vacated
  (\secref{sec:advance}). 
\end{enumerate}

After a faction has completed its three phases, it is the other factions
turn to execute its three phases.  This is summarised in
\tabref{tab:turn}.

\begin{table}
  \centering
  \begin{tabular}{|ll|}
    \hline
    \rowcolor{hostile!50!white}
    \multicolumn{2}{|l|}{Red turn}
    \\
    \defrow
    & Movement
    \\
    \rowcolor{hostile!15!white}
    & Combat
    \\
    \defrow
    & Advance
    \\
    \rowcolor{friendly!50!white}
    \multicolumn{2}{|l|}{Blue turn}
    \\
    \defrow
    & Movement
    \\
    \rowcolor{friendly!15!white}
    & Combat
    \\
    \defrow
    & Advance
    \\
    \hline
  \end{tabular}
  \caption{Turn sequence}
  \label{tab:turn}
\end{table}

%% -------------------------------------------------------------------
\subsection{Victory}

The first faction to eliminate its opponents headquarters (\hqmark)
wins the game \emph{immediately}.  If a faction believes that it is no
longer possible to win the game, then that, and only that, faction may
concede the game.

\paragraph{Stalemate} \textsl{(optional)} A faction may call
\emph{stalemate}.  If, after ten more turns after a faction has called
stalemate, no units have been eliminated, then the game ends in a
draw.

\paragraph{Timed game} \textsl{(optional)} Factions may decide to allot a
specific cumulative time for each faction --- say 30 minutes each (use
a chess clock or similar to keep track of the total time used by
either faction).  If a faction runs out of time, then that faction
automatically loses, even if the opposing faction did not eliminate
the factions headquarters.

\paragraph{Victory points} \textsl{(optional)} The factions must
decide a number of turns to play, say 15 turns.  If a faction
eliminates the opposing headquarters before the turns have run out,
then that faction wins the came \emph{immediately}, as per normal
(\emph{sudden death}).  If no faction has eliminated its opposing
headquarters by then of the last turn, then each faction tallies the
cost of its eliminated units (see the \emph{period army} table for per
unit costs).  The faction that has lost \emph{the least} cost wins the
game.  In case the sum costs of eliminate units are equal, then the
game is a draw.  This optional rule \emph{can} be combined with the
\emph{Timed game} operation rule, but \emph{not} with the
\emph{Stalemate} optional rule. 

%% ===================================================================
\section{Set-up}
\label{sec:setup}

%% -------------------------------------------------------------------
\subsection{Period}
\label{sec:setup:period}

The choice of period not only determines the ``flavour'' of the game,
but also which types of units the faction controls.  There are seven
possible periods to chose from, as detailed in
\secref{sec:period:basic} to \secref{sec:period:ancient}.  The periods
are


\begin{description}
\item[Basic] 
\item[Modern] 
\item[American civil war] 
\item[Napoleonic wars] 
\item[Pike and shot] 
\item[Medieval] 
\item[Ancient] 
\end{description}

Which period, and in some cases which kind of armies, chosen has a
major impact on the game played.  It is recommended to start out with
the simpler \textbf{Basic} period (\secref{sec:period:basic}) before
trying the more advanced periods.

While it is tempting to mix and match between periods, it is not
advisable.  Within each period, the armies have particular
capabilities which reflect the time, but which would not be the same
in a different historical period. 

%% -------------------------------------------------------------------
\subsection{Select armies and units}
\label{sec:setup:armies}

Once the period has been decided upon, the factions may
\emph{individually} determine which of the possible army to use.  For
example, if the \emph{Ancient} (\secref{sec:period:ancient}) period
was chosen, then the red faction may decide to use the Roman army
while the Blue faction chooses the Greek army.

The factions should take counters correspond to their army of choice,
with the appropriate colour, and put them next to the board.  The
counters are \emph{not} placed on the board until after the terrain
and tile set-up (\secref{sec:setup:tiles}).

The \emph{period card} of the chosen period details how many of each
kind of unit comprises an army.  For example, in the \emph{Basic}
period (\secref{sec:period:basic}) both factions receive 1
headquarters (\hqmark), 8 infantry (\infantrymark), 2 artillery
(\artillerymark), and 2 armoured (\armouredmark) units.

\paragraph{Buying armies} \textsl{(optional)}.  Instead of receiving a
fixed number of units of a given type, the factions may decide upon a
number \emph{resource points}, for example 100 points for the
\emph{Ancient} period, and 90 points for all other periods.  Each
faction must then expend these resource points to allocate armies.
The costs in resource points are listed in the \textbf{Cost} column of
the period cards for each type of unit.   Note, the factions
\emph{must} have a \emph{single} headquarter unit (at no cost), but
are otherwise free to mix-and-match as they see fit, up to the number
of available counters of a given kind.   These purchases should be
done in secret, but an opposing faction may challenge the validity
once the armies are revealed. 

%% -------------------------------------------------------------------
\subsection{Place tiles and headquarters}
\label{sec:setup:tiles}

Once the period has been agreed upon, and the factions chosen their
army setup (if applicable to the period), it is time to set up the
board --- in particular place terrain tiles \emph{and} the faction
headquarters.

There are several options for setting up the terrain tiles and
headquarters.

\paragraph{Standard} It is recommended to use start with the standard
setup shown in \figref{fig:setup:standard}.

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=.5*\wgscale]
    \begin{fullboard}
      \town(hex cs:c=3,r=2)
      \town(hex cs:c=9,r=10)
      \woods(hex cs:c=8,r=3,v=E)
      \woods[rotate=180](hex cs:c=4,r=10,v=W)
      \smallhill(hex cs:c=7,r=4,e=S)
      \smallhill(hex cs:c=5,r=8,e=N)
      \largehill(hex cs:c=9,r=7)
      \largehill[rotate=180](hex cs:c=3,r=5)
      \chit[r hq](hex cs:c=10,r=2);
      \chit[b hq](hex cs:c=2,r=11);
    \end{fullboard}
  \end{tikzpicture}
  \caption[Standard set-up]{Standard set-up of terrain tiles and
    headquarters.}
  \label{fig:setup:standard}
\end{figure}

\paragraph{Alternative setups} \textsl{(optional)} The faction may
agree to use one of the setups shown in \figref{fig:setup:a} to
\ref{fig:setup:c}.

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=.5*\wgscale]
    \begin{fullboard}
      \town(hex cs:c=6,r=3)
      \town(hex cs:c=6,r=10)
      \woods(hex cs:c=2,r=3,v=E)
      \woods[rotate=180](hex cs:c=10,r=10,v=W)
      \smallhill(hex cs:c=7,r=6,e=S)
      \smallhill(hex cs:c=5,r=6,e=N)
      \largehill[rotate=180](hex cs:c=5,r=7)
      \largehill(hex cs:c=7,r=5)
      \chit[r hq](hex cs:c=2,r=2);
      \chit[b hq](hex cs:c=10,r=11);
    \end{fullboard}
  \end{tikzpicture}
  \caption[Alternative setup A]{Alternative setup A of terrain tiles and
    headquarters.}
  \label{fig:setup:a}
\end{figure}

\paragraph{User defined} \textsl{(optional)}.  The faction may decide
to make their own terrain and feature configuration.  In that case,
the following limitations \emph{must} be observed

\begin{enumerate}
\item The terrain tiles \emph{must} be placed the same on either half
  of the board.  That is, the placement of tiles should be invariant
  with respect to a $180^{\circ}$ rotation. 
\item Terrain tiles or headquarters \emph{may not} be placed in a hex
  on one of the four edges of the board. 
\item No terrain tile may placed in a hex with a river hex side,
  including hexes 0408 and 0850. 
\item Terrain tiles of different terrain \emph{may not} be placed next
  to each other, e.g., a \emph{woods} tile may not be placed next to a
  town or hill tile. 
\item Headquarter units \emph{must} start behind the corresponding
  set-up line (the red and blue dashed lines), and \emph{must} be
  placed in \emph{Clear} terrain. 
\end{enumerate}

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=.5*\wgscale]
    \begin{fullboard}
      \town(hex cs:c=6,r=6)
      \town(hex cs:c=6,r=7)
      \woods(hex cs:c=2,r=6,v=E)
      \woods[rotate=180](hex cs:c=10,r=7,v=W)
      \smallhill(hex cs:c=6,r=2,e=S)
      \smallhill(hex cs:c=6,r=11,e=N)
      \largehill[rotate=180](hex cs:c=6,r=9)
      \largehill(hex cs:c=6,r=4)
      \chit[r hq](hex cs:c=2,r=2);
      \chit[b hq](hex cs:c=10,r=11);
    \end{fullboard}
  \end{tikzpicture}
  \caption[Alternative setup B]{Alternative setup B of terrain tiles and
    headquarters.}
  \label{fig:setup:b}
\end{figure}

\paragraph{Faction defined} \textsl{(optional)}.  The faction may decide
to make their own terrain and feature configuration.  In that case,
the following limitations \emph{must} be observed.

\begin{enumerate}
\item Each faction takes half of the tiles: one town, one small and
  one large hill, and one woods tile.
\item The tiles and the factions headquarter unit are placed in
  \emph{secret}, for example by putting up a screen between the two
  halves of the board. 
\item Tiles \emph{must} be placed \emph{entirely} on the factions side
  of the board. 
\item The limitations, except the first, given above for the
  \textbf{User defined} setup optional rule \emph{must} be observed. 
\end{enumerate}

When both factions have set-up the tiles and its headquarter unit,
then the factions reveal the set-up to each other.  The factions
should take enough time to familiarise themselves with their opponents
set-up.   Then, the units of both factions are set-up as per normal,
i.e., in secret. 

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=.5*\wgscale]
    \begin{fullboard}
      \town(hex cs:c=3,r=9)
      \town(hex cs:c=9,r=3)
      \woods(hex cs:c=2,r=5,v=E)
      \woods[rotate=180](hex cs:c=10,r=8,v=W)
      \smallhill[rotate=-60](hex cs:c=5,r=6,e=NE)
      \smallhill[rotate=120](hex cs:c=7,r=6,e=SW)
      \largehill[rotate=180](hex cs:c=6,r=3)
      \largehill(hex cs:c=6,r=10)
      \chit[r hq](hex cs:c=6,r=2);
      \chit[b hq](hex cs:c=6,r=11);
    \end{fullboard}
  \end{tikzpicture}
  \caption[Alternative setup C]{Alternative setup C of terrain tiles and
    headquarters.}
  \label{fig:setup:c}
\end{figure}

\paragraph{Rivers} \textsl{(optional)}.  The factions may decide to
modify the two rivers in \emph{one} of the following ways.

\begin{itemize}
\item Ignore rivers.  Movement is \emph{not} hindered by rivers
  (\secref{sec:movement}). 
\item Ignore bridges.  It is not possible to cross a river hex side,
  even if there is a bridge across that hex side. 
\item Ignore part of the rivers.  Crossing specific river hex sides
  does not incur any penalty.   The river hex sides ignored should be
  the same on both halves of the board.  For example, 0307--0308
  \emph{and} 0904--0905 are \emph{both} not considered river hex
  sides. 
\end{itemize}

%% -------------------------------------------------------------------
\subsection{Place units}
\label{sec:setup:units}

When the terrain tiles and headquarter units have been placed on the
board, it is time for each faction to deploy its units \emph{in
  secret}.  To do this, a screen should be set-up over the middle of
the board.

Each faction then places its units with the following limitations.

\begin{enumerate}
\item \emph{All} units \emph{must} be put on the board. 
\item \emph{At most} one unit in hex. 
\item Units can be placed on terrain tiles (including town), but
  \emph{not} in the same hex as headquarters (\hqmark). 
\item Units \emph{must} be placed behind the faction coloured set-up
  line. 
\end{enumerate}

\paragraph{Fog of war} \textsl{(optional)}.  The type of units is not
known initially to the opposing faction.   Before revealing the set-up
to the opposing faction, all units are flipped face down so as not to
reveal their identity.   Units are flipped to their face-up side when
ever an enemy unit comes within two hexes (one intermittent hex) of a
unit.    See also \figref{fig:setup:fow}.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{boardexcerpt}(hex cs:c=4,r=9)(hex cs:c=8,r=4)
      \chit[b back](hex cs:c=6,r=8);\shadechit[.5](hex cs:c=6,r=8);
      \draw[hex/move=friendly!50!black](hex cs:c=6,r=8)--(hex cs:c=6,r=7);
      \chit[b inf](hex cs:c=6,r=7);
      \chit[r inf](hex cs:c=5,r=5);
      \chit[r arty](hex cs:c=6,r=5);
      \chit[r recce](hex cs:c=7,r=5);
      \chit[r back](hex cs:c=5,r=4);
    \end{boardexcerpt}
  \end{tikzpicture}
  \caption[Fog of War.]{Fog of War.  The blue unit moves to 0607.
    Since it is two or less hexes away from enemy units, it is flipped
    over to reveal that it is an infantry unit.  Likewise, the red
    units in 0505, 0605, and 0705 are revealed.  The red unit in 0504
    is \emph{not} revealed as it is more than two hexes away. }
  \label{fig:setup:fow}
\end{figure}

Once the factions have placed \emph{all} their units on the board,
they reveal their set-up to each other.  opposing faction may at this
point object to wrongful set-ups.

When all possible disputes have been settled, the game moves on to the
first turn.  The first phase is the red \emph{Movement} phase
(\secref{sec:movement}).

%% ===================================================================
\section{Movement}
\label{sec:movement}

In a factions movement phase, that faction, and \emph{only} that
faction, may move as few, as many, or none at all, of the factions
units.   The movement allowance of the units is given in the
\emph{period card} for each particular kind of unit.  Movement
\emph{must} adhere to the following limitations.

\begin{enumerate}
\item A unit may never move more hexes than its movement allowance.
  It may, however, move \emph{less}. 
\item Movement allowance is \emph{not} transferable between units,
  \emph{nor} can movement allowance be saved for use in a later turn. 
\item A unit may \emph{never} move into a hex occupied by an enemy
  unit. 
\item Depending on the period, some unit types \emph{cannot} enter
  specific kinds of terrain.  This is noted by a `--' in the period
  card. 
\item Units may \emph{not} leave the board. 
\end{enumerate}

\paragraph{Timed moves} \textsl{(optional)}. The factions may decide
to limit the time a faction has to perform its moves, for example 2
minutes.  When the time is up, no more moves can be made by the
faction. 

%% -------------------------------------------------------------------
\subsection{Stacking}
\label{sec:movement:stack}

At the \emph{end} of the movement phase \emph{no more} than one unit
may occupy a hex.   Units may move through hexes occupied by friendly
units --- including headquarters ---, or swap places as long as only
one unit is in given hex at the end of the movement phase. 

%% -------------------------------------------------------------------
\subsection{Rivers}
\label{sec:movement:rivers}

Units may \emph{only} move a cross a river hex side if that hex side
has a bridge symbol on it (0208 to 0209, 1004 to 1005, and vice
versa).

\paragraph{Stop on crossing} \textsl{(optional)}.  With this optional
rule in force, a unit \emph{may} cross a river hex side but
\emph{must} stop movement after crossing the river.  Crossing over a
bridge \emph{does not} hinder further movement.

\paragraph{No attack on crossing} \textsl{(optional)}.  A unit may
cross a river hex side, but \emph{cannot} conduct an attack that turn.
This can be used in \emph{addition} to the optional rule \textbf{Stop
  on crossing} above (i.e., a unit may \emph{not} continue movement,
\emph{nor} attack on the turn it crosses a river hex side), or
independently (i.e., a unit may continue movement after crossing but
cannot attack).

%% -------------------------------------------------------------------
\subsection{Strategic movement}
\label{sec:movement:strategic}

In some periods, some unit types, marked by a \textsuperscript{*} in
the \textbf{Move} column, may perform \emph{strategic movement}.
A unit performing strategic movement 
\begin{itemize}
\item has its movement allowance increased to the started
  (\textsuperscript{*}) value in the period card, \emph{but} 
\item may \emph{not} attack during that turn. 
\end{itemize}

Thus, a reconnaissance unit (\reconnaissancemark) in the \emph{Modern}
period may move 3 hexes \emph{if, and only if} it does not attack in
the factions \emph{Combat} phase (\secref{sec:combat}).

%% ===================================================================
\section{Combat}
\label{sec:combat}

Combat always takes place between \emph{adjacent} opposing units.
Thus, if and the start of a combat phase the current faction has some
of its units adjacent to enemy units, then those units \emph{may}
engage in combat with the enemy.   Units are \emph{never} forced to
attack adjacent enemy units.

Combats are declared and resolved in \emph{any} order as decided by
the faction in turn (e.g., the red faction during the red combat
phase).  Combats a resolved with the following limitations.

\begin{enumerate}
\item A unit may attack only \emph{once} per turn. 
\item An enemy unit may defend against an attack only \emph{once} per
  turn. 
\item A combat is always against an enemy unit in a \emph{single}
  hex. 
\item Attacking units from multiple hexes may attack together against
  a defender in a single hex if and only if all attackers are adjacent
  to the defending unit. 
\item Units \emph{may} attack into a hex it cannot otherwise move
  into.  For example, in the \emph{Basic} period an armoured
  (\armouredmark) may not move into a woods hex, but may attack an
  enemy unit in that hex.  The optional rule \textbf{Forced advance}
  changes this rule (\secref{sec:advance}). 
\item Units may \emph{not} attack river hex sides, \emph{even} if
  connected via a bridge.  The optional rules \textbf{Storming the
    bridge} and \textbf{To the other shore} modify this rule. 
\item The combat strength of a unit \emph{cannot} be divided up,
  \emph{nor} can it be saved for use in a later turn. 
\end{enumerate}

See also \figref{fig:combat:1} for an example.
\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{boardexcerpt}(hex cs:c=4,r=7,e=N)(hex cs:c=8,r=5,e=N)
      \town(hex cs:c=3,r=2)
      \town(hex cs:c=9,r=10)
      \woods(hex cs:c=8,r=3,v=E)
      \woods[rotate=180](hex cs:c=4,r=10,v=W)
      \smallhill(hex cs:c=7,r=4,e=S)
      \smallhill(hex cs:c=5,r=8,e=N)
      \largehill(hex cs:c=9,r=7)
      \largehill[rotate=180](hex cs:c=3,r=5)
      \chit[r arty](hex cs:c=7,r=5);
      \chit[r arm](hex cs:c=6,r=5);
      \chit[r inf](hex cs:c=5,r=5);
      \chit[b inf](hex cs:c=6,r=6);
      \draw[hex/attack] (hex cs:c=7,r=5)--(hex cs:c=6,r=6);
      \draw[hex/attack] (hex cs:c=6,r=5)--(hex cs:c=6,r=6);
    \end{boardexcerpt}
  \end{tikzpicture}
  \caption[An example of combat]{An example of combat in the
    \emph{Basic} period.  The red artillery on a hill (\CF[2]) and
    armoured (\CF[2]) units attack a blue infantry unit in the open
    (\CF[1]). The red infantry unit in 0505 does not participate in
    the combat.  The combat is \CF[4] versus \CF[1], and so the blue
    infantry unit is eliminated.}
  \label{fig:combat:1}
\end{figure}

\paragraph{Storming the bridge} \textsl{(optional)}.  Units \emph{may}
attack across a bridge (0208 to 0209, 1004 to 1005, or vice versa), but
the attacking units combat strength is reduced by one.  For example,
and armoured (\armouredmark) unit in the \emph{Basic} period attacking
across a bridge has its combat strength reduced from 2 to 1.  Note
that \emph{only} the unit attacking across the bridge suffers the
penalty. If the defender is attack from some other hex, then those
attacking units \emph{do not} suffer the -1 penalty. 

\paragraph{To the other shore} \textsl{(optional)}.  Units \emph{may}
attack across a river hex side, but they suffer a -2 penalty to its
combat strength.  For example a Napoleonic cavalry (\mechinfantrymark)
unit attacking across a river hex side has its strength reduced from 3
to 1.  Only the units that attack across a river hex side has their
combat strength reduced.  Other units that do not attack across a
river hex side (or bridge) do not have their strength reduced.  It is
highly recommended that the above \textbf{Storming the bridge}
optional rule is used with this optional rule.  In that case, units
attacking over a bridge suffer a -1 penalty while units attacking over
a river hex side with no bridge suffer a -2 penalty.  See also
\figref{fig:combat:river} for an example.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{boardexcerpt}(hex cs:c=1,r=9)(hex cs:c=4,r=8,e=N)
      \town(hex cs:c=3,r=2)
      \town(hex cs:c=9,r=10)
      \woods(hex cs:c=8,r=3,v=E)
      \woods[rotate=180](hex cs:c=4,r=10,v=W)
      \smallhill(hex cs:c=7,r=4,e=S)
      \smallhill(hex cs:c=5,r=8,e=N)
      \largehill(hex cs:c=9,r=7)
      \largehill[rotate=180](hex cs:c=3,r=5)
      \chit[r inf](hex cs:c=2,r=8);
      \chit[b recce](hex cs:c=3,r=8);
      \chit[b arty](hex cs:c=2,r=9);
      \draw[hex/attack] (hex cs:c=3,r=8)--(hex cs:c=2,r=8);
      \draw[hex/attack] (hex cs:c=2,r=9)--(hex cs:c=2,r=8);
    \end{boardexcerpt}
  \end{tikzpicture}
  \caption[An example of river combat]{An example of river combat. In
    the Napoleonic period, the blue cavalry (\mechinfantrymark,
    \CF[$3-2=1$]) attacks the opposing shore, while the blue artillery
    supports across the bridge (\artillerymark, \CF[$2-1=1$]).  The
    defender is blue infantry (\infantrymark, \CF[2] because at least
    one attacker is \mechinfantrymark{} or \reconnaissancemark{}).}
  \label{fig:combat:river}
\end{figure}

%% -------------------------------------------------------------------
\subsection{Combat strength (\CF)}
\label{sec:movement:cf}

All units have a combat strength that depend on the kind of terrain it
occupies.   This is shown in the \emph{period card}.  The combat
strength is the same whether attacking \emph{or} defending, with a few
exceptions.  For example an infantry unit on a hill in the
\emph{American civil war} period has a combat strength of 3.

In some cases, as marked by a \againstamark{} in the \emph{period
  card}, a units combat strength is different if \emph{any opposing}
unit in the combat, attacking \emph{or} defending, is of a certain
kind.  For example, in the \emph{Basic} period, an artillery unit
occupying a hill hex, in combat with an opposing infantry unit has a
combat strength of \CF[2], as shown in \figref{fig:combat:1}.

For some periods, the combat strength of a unit may also be modified
if the unit is \emph{defending} against units of a certain type.  This
is indicated by a \againstbmark{} in the \emph{period card}.  If
\emph{any} attacking unit is of the indicated type, then that combat
strength applies. For example, a Napoleonic infantry unit in a clear
hex, \emph{defending} against \emph{any} number of cavalry
(\mechinfantrymark) or light cavalry (\reconnaissancemark) units has a
combat strength of \CF[2].  An example is shown in
\figref{fig:combat:river}.

If there is a conflict between the combat factors given by the
\againstamark{} and \againstbmark{} combat strengths, then the
\emph{lower} value is used.

%% -------------------------------------------------------------------
\subsection{Resolve combat}
\label{sec:movement:resolve}

The combat strengths of all attacking units are summed up and compared
to the combat strength of the defending unit.

\begin{itemize}
\item If the total attacking combat strength is \emph{larger} than the
  defending combat strength, then the defending unit is
  \emph{eliminated}. An example of this situation is shown in
  \figref{fig:combat:1}.
\item If the total attacking combat strength is \emph{equal} to the
  defending combat strength, then the combat has no effect.  An
  example of this situation is shown in \figref{fig:combat:river}.
\item If the total attacking combat strength is \emph{smaller} than
  the defending combat strength, then \emph{all} attacking units are
  eliminated\footnote{Since combat is \emph{voluntary} it seems
    unlikely that a faction will want to attack where this would be
    the result. However, a faction may want to deliberately place
    units such that the opposing faction may attack that unit and
    eliminate it, perhaps to block off advances.}.
\end{itemize}

Eliminated units are removed from the board.  Eliminated \emph{may
  not} enter the board again. 

%% -------------------------------------------------------------------
\section{Advances}
\label{sec:advance}

After \emph{all} combat have been resolved, then the faction in turn
\emph{may} advance victorious units into hexes vacated as a result of
combat.  Advances are subject to the following constraints.
\begin{enumerate}
\item \emph{Only} units that attacked and eliminated the enemy unit
  may advance. 
\item \emph{At most} one unit may advance into the hex vacated by the
  enemy. 
\item A unit \emph{may not} advance into terrain it is otherwise
  prohibited from entering.  E.g., an armoured unit (\armouredmark) in
  the \emph{Basic} period may not advance into a woods hex. 
\item Unit may \emph{never} advance into a hex occupied by enemy
  units. 
\item At the end of this phase, \emph{at most} one unit may occupy any
  single hex. 
\end{enumerate}

\paragraph{Forced advance} \textsl{(optional)}.  If this optional rule
is in force, then advances are \emph{never} voluntary.  This
\emph{does not} mean that a unit may enter terrain it otherwise could
not enter.   Therefore, if it is not possible for an attacking faction
to advance \emph{any} unit into a vacated hex after combat, then that
attack is not possible.  For example, in the \emph{Basic} period, a
sole armoured unit (\armouredmark) may \emph{not} attack an enemy in a
woods hex.  If, however, the armoured unit is joined in the attack by
an infantry unit (\infantrymark), then the two units \emph{may} attack
into the woods hex. 

%% LocalWords:  grognards
%%
%% Local Variables:
%% mode: LaTeX
%% TeX-master: "Battle.tex"
%% TeX-engine: luatex
%% End:

